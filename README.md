# Hello World!

This is my first repository. I created this repo to help me understand Markdown and the inner workings of GitLab. As a way to help me learn Markdown, I made this list of my favorite things. Enjoy!

## Favorite Color

Purple

## Favorite Food

Fried rice

## Favorite Vegetable

Asparagus

## Favorite Fruit

Mango

## Favorite Soda

Dr. Pepper and Sprite (tie)

## Favorite Fast Food Restaurant

Checkers and Rally's

## All-Time Favorite TV Show

_Teen Titans_

## Favorite TV Show I'm Currently Watching Now

_Abbott Elementary_

## Favorite Anime Series

_Kill la Kill_

## Favorite Book Series

_Artemis Fowl_ by Eoin Colfer

![Artemis Fowl #1 cover](https://vignette.wikia.nocookie.net/artemisfowl/images/3/37/Images-1.jpeg)

## Favorite Graphic Novel Series

_Amulet_ by Kazuhiro "Kazu" Kibuishi

![Amulet: The Stonekeeper](https://upload.wikimedia.org/wikipedia/en/5/5f/The_Stonekeeper.jpg)

## Favorite Music Band/Group

CHVRCHΞS

## Favorite Singer

The Weeknd

## Favorite Movie

_Spider-Man: Into the Spider-Verse_ (2018)

![Movie poster](https://upload.wikimedia.org/wikipedia/en/f/fa/Spider-Man_Into_the_Spider-Verse_poster.png)

## Favorite Pixar Movie

_Ratatouille_ (2007)

## Favorite Actor

Patrick Stewart

## Favorite Actress

Keke Palmer

## Favorite Comedian

Stephen Colbert

> I'm not here to affect you politically or socially. I'm here to make you laugh. I use the news as the palette for my jokes.
>
> [Source](https://www.nj.com/entertainment/tv/2009/10/stephen_colbert_interview_s_pe.html)

## Favorite Superhero

Kamala Khan / Ms. Marvel
